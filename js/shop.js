    $(document).ready(function () {
        $('[data-toggle="popover"]').popover({
           html:true
       });
        
        var maxLength = 255;
        $('textarea').keyup(function() {
         var length = $(this).val().length;
         var length = maxLength-length;
         $(this).parent().find('.chars').text(length);
     });

        $('#show-date-advance').click(function(){
           $('#switch-components').slideToggle();
           $('#operation-hour').slideToggle();
           if( !$(this).hasClass('active')){
              $(this).addClass('active');
              $(this).html('Hide advance Options <i class="fa fa-chevron-down" aria-hidden="true"></i>' );
          }else{
              $(this).removeClass('active');
              $(this).html('Show advance Options <i class="fa fa-chevron-up" aria-hidden="true"></i>' );     
          }
      });
    });

    function add_store(that){
     $( "#default-map" ).clone().appendTo( "#lastchild" );
     var mapid = "map" + $('#lastchild .form-group').length;
     $('#lastchild .form-group').last().attr('id', mapid);

     var iframemapid = "#" + mapid;
     $(iframemapid).hide();
 }

 function confirm_addfloor(){
     var mapid = "map" + $('#lastchild .form-group').length;
     var storeName = "Store " + $('#lastchild .form-group').length;
     var iframemapid = "#" + mapid;
     $(iframemapid).show();
     $(iframemapid).find('.shop-title').css('float', 'left').html('Shop Title: ');
     $(iframemapid).find('.shop-title').next('div').find('input').show().val(storeName);
     $(iframemapid).find('.delete-floor').html('<label> Action: </label> <div> <button type="button" class="btn btn-danger"  data-toggle="modal" data-target="#delete_store_popup"> Delete </button> </div>');
     $('#delete_store .store_delete_id').html(mapid);
     $('[data-toggle="popover"]').popover({
      html:true,
      content: 'Are you sure add new shop?<a onclick="cancel_addFloor(this);"> <u style="color: #1ea500;">Yes</u> </a> &nbsp <a onclick="close_popover(this);"> <u style="color: #FF0000;"> No </u>  </a>'
  });
     $('[data-toggle="popover"]').popover('hide');
     var c = $(iframemapid + ' .iframe_map_target').contents();
     c.find('.points').attr('data-targetmap', iframemapid);
     $('#more-map').show();
 }

 function cancel_addFloor(){
     var mapid = "map" + $('#lastchild .form-group').length;
     var iframemapid = "#" + mapid;
     $(iframemapid).remove();
     $('[data-toggle="popover"]').popover('hide');
 }

 function delete_store_popup(that){
     var target = '#' + $("#delete_store_popup .store_delete_id").html();
     $(target).remove();
     $("#delete_store_popup").modal('hide');
 }
// END of elTOV Singapore Update

function setMainCateChg(){
	var obj = document.getElementById("id_cate_code");
	var index_value = obj.selectedIndex;
	var cate_code = obj.options[index_value].value;

	if(cate_code == "S10"){
		document.getElementById("id_food_field01").style.display = "";
		document.getElementById("id_food_field02").style.display = "";
		document.getElementById("id_food_field03").style.display = "";
	}else{
		document.getElementById("id_food_field01").style.display = "none";
		document.getElementById("id_food_field02").style.display = "none";
		document.getElementById("id_food_field03").style.display = "none";
	}
}

function setValueCheck(){
	var f = document.frmwrite;
	var store_type              = "";
	var user_type               = "";
	var store_tag               = "";
	var store_servicetime_stime = "";
	var store_servicetime_etime = "";
	var store_servicetime       = "";
	var search_type             = "";
	var display_type            = "";
	var names = document.querySelectorAll(".input-name");
	var forms = document.querySelectorAll(".form-status");
	var main_file = $('#id_main').val();
	var logo_file = $('#id_logo').val();

	display_type            = $('#id_display_type').val();
	store_servicetime_stime = $('#id_sch_time1').val();
	store_servicetime_etime = $('#id_sch_time2').val();
	store_servicetime       = store_servicetime_stime + "-" + store_servicetime_etime;


        // 매장 표시 체크
        if(display_type == ""){
        	alert('Please enter store Display/Hide');
        	$('#id_display_type').focus();
        	return;  
        }else{
        	if(display_type=="MAP"){
        		display_type = "Y";
        		search_type  = "N";
        	}else if(display_type=="LIST"){
        		display_type = "N";
        		search_type  = "Y";
        	}else if(display_type=="HIDE"){
        		display_type = "N";
        		search_type  = "N";
        	}else if(display_type=="SHOW"){
        		display_type = "Y";
        		search_type  = "Y";
        	}
        }

        
        if($(".checked input[name=tmp_store_type01]").val() == "01"){
        	if(store_type == "") store_type = "01";
        	else store_type = store_type + ",01";
        }

        if($(".checked input[name=tmp_store_type02]").val() == "02"){
        	if(store_type == "") store_type = "02";
        	else store_type = store_type + ",02";
        }

        if($(".checked input[name=tmp_store_type03]").val() == "03"){
        	if(store_type == "") store_type = "03";
        	else store_type = store_type + ",03";
        }

        if($(".checked input[name=tmp_store_type04]").val() == "04"){
        	if(store_type == "") store_type = "04";
        	else store_type = store_type + ",04";
        }

        // 유저 타입 체크
        if($(".checked input[name=tmp_user_type_1]").val() == "MY"){
        	if(user_type == "") user_type = "MY";
        	else user_type = user_type + ",MY";
        } 
        if($(".checked input[name=tmp_user_type_2]").val() == "MS"){
        	if(user_type == "") user_type = "MS";
        	else user_type = user_type + ",MS";
        } 
        if($(".checked input[name=tmp_user_type_3]").val() == "WY"){
        	if(user_type == "") user_type = "WY";
        	else user_type = user_type + ",WY";
        } 
        if($(".checked input[name=tmp_user_type_4]").val() == "WS"){
        	if(user_type == "") user_type = "WS";
        	else user_type = user_type + ",WS";
        } 
        if($(".checked input[name=tmp_user_type_5]").val() == "UY"){
        	if(user_type == "") user_type = "UY";
        	else user_type = user_type + ",UY";
        } 
        if($(".checked input[name=tmp_user_type_6]").val() == "US"){
        	if(user_type == "") user_type = "US";
        	else user_type = user_type + ",US";
        } 

        // 매장정보중에 하나라도 입력을 안했을경우, store_status를 D로 해준다.
        var store_status="A";
        for(var i=0; i<forms.length; i++){
        	if(forms[i].value == ""){
        		store_status ="D";
        	}
        }
        if(store_type =="" || user_type ==""){
        	store_status ="D";
        }
        
        // 태그 리스트 가져오기
        var arr_tag = new Array();
        $('.clas_tag').each(function(){
        	arr_tag.push($(this).html())
        });
        store_tag = arr_tag.toString()
        store_tag = store_tag.replace(/,/g,"");
        store_tag = store_tag.substring(1,store_tag.length);
        
        if(confirm("Would you like to register?") == true){
        	$('#loading').fadeIn();
        	f.store_type.value    = store_type;
        	f.user_type.value     = user_type;
        	f.display_type.value  = display_type;
        	f.search_type.value   = search_type;
        	f.store_status.value  = store_status;
        	f.store_tag.value     = store_tag;
        	f.store_servicetime.value = store_servicetime;
        	f.target = "frameupdate";
        	f.submit();
        }
        
    }

    function setListMove(){
    	document.frmpage.action = "store_list.jsp";
    	document.frmpage.submit();
    }

    function setPageDelete(){
    	var f = document.frmwrite;
    	if(confirm("Delete?") == true){
    		f.mode.value = "DELETE";
    		$('#loading').fadeIn();
    		f.target = "frameupdate";
    		f.submit();
    	}
    }

    // function setFloorChange(){
    //     var obj = document.getElementById("id_floor_code");
    //     var index_value = obj.selectedIndex;
    //     var floor_code = obj.options[index_value].value;

    //     var pos_x = document.getElementById("id_pos_x").value;
    //     var pos_y = document.getElementById("id_pos_y").value;
    //     var pos_help_x = document.getElementById("id_pos_help_x").value;
    //     var pos_help_y = document.getElementById("id_pos_help_y").value;

    //     document.getElementById("iframe_map").src = "../include/mapstore.jsp?floor_code=" + floor_code + "&x=" + pos_x + "&y=" + pos_y + "&help_x=" + pos_help_x + "&help_y=" + pos_help_y + "&scale=0.25&javascript_scale=4";
    // }

    function setShow(num){  //맵 확대 / 축소
    	var obj = document.getElementById("id_floor_code");
    	var index_value = obj.selectedIndex;
    	var floor_code = obj.options[index_value].value;
    	var pos_x = document.getElementById("id_pos_x").value;
    	var pos_y = document.getElementById("id_pos_y").value;
    	var pos_help_x = document.getElementById("id_pos_help_x").value;
    	var pos_help_y = document.getElementById("id_pos_help_y").value;

    	if(floor_code != ""){
    		if(num==1){
    			document.getElementById("iframe_map").src = "../include/mapstore.jsp?floor_code=" + floor_code + "&x=" + pos_x + "&y=" + pos_y + "&help_x=" + pos_help_x + "&help_y=" + pos_help_y + "&scale=0.5&javascript_scale=2";
    		}else if(num==2){
    			document.getElementById("iframe_map").src = "../include/mapstore.jsp?floor_code=" + floor_code + "&x=" + pos_x + "&y=" + pos_y + "&help_x=" + pos_help_x + "&help_y=" + pos_help_y + "&scale=0.25&javascript_scale=4";
    		}
    	}
    }

    //카테고리 값을 바꾸어 주는 부분
    var optionArray = new Array();

    optionArray[0] = ['S01','Jewellery and Watches'];
    optionArray[1] = ['S02','Beauty and Wellness'];
    optionArray[2] = ['S03','Fashion'];
    optionArray[3] = ['S04','Handbags, Shoes and Accessories'];
    optionArray[4] = ['S05','Sports'];
    optionArray[5] = ['S06','Lifestyle and Gifts'];
    optionArray[6] = ['S07','Children and Maternity'];
    optionArray[7] = ['S08','Optical'];
    optionArray[8] = ['S09','Confectionery & Deli'];
    optionArray[9] = ['S10','Dine','Restaurant','Fast Food','Cafe','Quick Bites','Food Court'];
    optionArray[10] = ['S11','Supermarket'];
    optionArray[11] = ['S12','Cinema'];
    optionArray[12] = ['S13','General Services'];
    optionArray[13] = ['S14','Local Gifts'];
    optionArray[14] = ['S15','Extended Hours'];
    optionArray[15] = ['S16','24 Hours'];

    $('#id_cate_code').on("change",function(){
    	var ck_val = $(this).val();
    	var contain;
    	console.log(ck_val);

    	if( ck_val == "S10"){
    		$('#id_subcate_code').children().remove();
    		for(var i=0; i<optionArray.length; i++){
    			if(ck_val == optionArray[i][0]){
    				$('#id_subcate_code').append(contain);
    				for(var k=2; k<optionArray[i].length; k++){
    					var contain = "<option value='"+ck_val+(k-2)+"'>"+optionArray[i][k]+"</option>";
    					$('#id_subcate_code').append(contain);
    				}
    			}
    		}
    	}else{
    		contain = "<option value=''>No Sub category</option>";
    		$('#id_subcate_code').children().remove();
    		$('#id_subcate_code').append(contain);
    	}
    });

    $('.files1').change(function(e){
    	$('.addon1').text($(this).val());
    });

    $('.files2').change(function(e){
    	$('.addon2').text($(this).val());
    });

    var main_cate_code = "S10";
    var sub_cate_code = "S102";
    if(main_cate_code != ""){
    	for(var i=0; i<optionArray.length; i++){
    		if(main_cate_code == optionArray[i][0]){
    			$('#id_subcate_code').children().remove();
    			for(var k=2; k<optionArray[i].length; k++){
    				if(sub_cate_code == main_cate_code+(k-2)){
    					var contain = "<option value='"+main_cate_code+(k-2)+"' selected>"+optionArray[i][k]+"</option>";
    				}else{
    					var contain = "<option value='"+main_cate_code+(k-2)+"'>"+optionArray[i][k]+"</option>";
    				}

    				$('#id_subcate_code').append(contain);
    			}
    		}
    	}
    }
    
    // 태그 입력
    function setStoreTag(){
    	var tag_index = $ ('.clas_tag').length;
        var store_tag = $('#id_store_tag').val();      // 태그 input
        var tag_data  = $('id_tag_data').html();       // 태그 입력시 나타내지는곳
        
        store_tag = store_tag.replace(/\s/gi,"");      // 모든공백제거
        store_tag = store_tag.replace(/#/g,"");        // # 막기
        
        if(store_tag != ""){
        	if(tag_index < 10){
                store_tag = "#"+store_tag;             // 데이터 앞에 # 입력
                $("#id_tag_data").append("<span class=\"clas_tag\" onclick=\"setStoreTagDel(this)\">"+store_tag+"</span>");    
            }else{
            	alert("You can not register more than 10 tags.");
            	return;
            }
        }else{
        	return;
        }

        // input에 데이터를 입력하고 리셋
        $('#id_store_tag').val("");
    }


    // 태그 삭제
    function setStoreTagDel(event){
    	$(event).remove();
    }
    
    // 태그 리셋
    function setStoreTagReset(){
    	$('.clas_tag').remove();
    }

    // 태그 필터링
    function setFilter(event){
    	var code= event.keyCode;
        // 51 => #
        if(code == 51){
        	event.returnValue = false;
        }

    }

    jQuery(document).ready(function(){
    	$('.radiobox_flat input').iCheck({ checkboxClass: 'icheckbox_flat-blue',radioClass: 'iradio_flat-blue',increaseArea: '10%'});

    	$('.fancybox').each(function(p_id){
    		var one = $(this).attr('href').substr($(this).attr('href').length-3 , $(this).attr('href').length);
    		var ext = one.toLowerCase();
    		var src = $(this).attr('href');
    		var img_id = 'imgViewer' + p_id;
    		if(ext == "flv" || ext == "mp4" || ext == "swf"){
    			var contents = "<div id='"+img_id+"' style='padding:0px !important;width:750px;text-align:center;overflow:hidden'><video width='700' height='500' controls><source src="+src+" type='video/mp4'> </video>  </div>";
    			$(this).fancybox({
    				openEffect : 'elastic',
    				closeEffect : 'elastic',
    				helpers: {title : {type : 'inside'}},
    				content:contents,
    				afterShow : function(){}
    			});
    		}else{
    			$(this).fancybox({openEffect:'elastic',closeEffect:'elastic',helpers:{title:{type:'inside'}}});
    		}
    	});

      // $('#id_sch_time1').timepicker({ defaultTime: "00:00", autoclose: true, minuteStep: 5, showSeconds: false, showMeridian: false });
      // $('#id_sch_time2').timepicker({ defaultTime: "23:59", autoclose: true, minuteStep: 5, showSeconds: false, showMeridian: false });

      $('.checkbox input').on('ifChecked', function(event){
      	$(this).parents('.clearfix').find('label.control-label, .select-time').hide();
      });

      $('.checkbox input').on('ifUnchecked', function(event){
      	$(this).parents('.clearfix').find('label.control-label, .select-time').show();
      });

      $('#id_sch_time3').on('ifChecked', function(event){
      	$('#id_sch_time3').val("Y");
      	$('.select-time').fadeOut();
      	$('#operation-hour #id_time_label1').fadeOut();
      	$('#operation-hour #id_time_label2').fadeOut();
      	$('#show-date-advance').fadeOut();
      });

      $('#id_sch_time3').on('ifUnchecked', function(event){         
      	$('#operation-hour #id_time_label1').fadeIn();
      	$('#operation-hour #id_time_label2').fadeIn();
      	$('.select-time').fadeIn();
      	$('#show-date-advance').fadeIn();
      });

      var i=0;
      var tag_list = "tag test#태그#태그입니다.";
      var arr_tags = tag_list.trim().split("#");

        // DB에있는 태그 가져오기
        for(i=0; i<arr_tags.length; i++){
        	$("#id_tag_data").append("<span class=\"clas_tag\" onclick=\"setStoreTagDel(this)\">"+"#"+arr_tags[i]+"</span>");    
        }

        // 태그 앤터
        $('#id_store_tag').keypress(function(event){
        	if ( event.which == 13 ) {
        		$('#id_tag_btn').click();
        		return false;
        	}
        });
    });