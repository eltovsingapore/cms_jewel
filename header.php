<!DOCTYPE html>
<!-- saved from url=(0176)http://jewel.tovms.com/stadmin/contents/store_info.jsp?store_id=35&store_name=&pageno=&pagesize=20&search_cate=&search_floor=&search_name=&display_type=&s_store_status=&s_sort= -->
<html class="">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta http-equiv="CACHE-CONTROL" content="NO-CACHE">
	<meta name="author" content="ThemeBucket">
	<title>All in one IT Report system(JewelMall)</title>
	<link rel="shortcut icon" type="image/png" href="/logo.png"/>
	<link rel="shortcut icon" type="image/png" href="http://jewel.tovms.com/zcommonfiles/branch/JEW//B0000000002_MAIN.png"/>

	<!--Core CSS -->
	<link href="./components/bootstrap.min.css" rel="stylesheet">
	<link href="./components/jquery-ui-1.10.1.custom.min.css" rel="stylesheet">
	<link href="./components/bootstrap-reset.css" rel="stylesheet">

	<script src="./components/commlang.js"></script><!--언어팩-->
	<script src="./components/jquery.js"></script><!--j쿼리-->
	<script src="./components/jquery-ui-1.10.1.custom.min.js"></script> <!--j쿼리ui-->
	<script src="./components/bootstrap.min.js"></script> <!--부트스트랩-->
	<script src="./components/jquery.dcjqaccordion.2.7.js"></script> <!--매뉴 효과--> 
	<script src="./components/jquery.nicescroll.js"></script> <!--매뉴 효과-->
	<script src="./components/jquery.scrollTo.js"></script>  <!--스크롤 기능-->
	<script src="./scripts.js"></script>  <!--함수를 빼놓은 부분-->
	<script src="./components/jquery.ui.touch-punch.js"></script>  <!--모바일에서의 모션을 감지하는 스크립트-->
	<!--파일 인풋 box-->
	<script type="text/javascript" src="./components/fileinput.js"></script>
	<link href="./components/fileinput.css" rel="stylesheet">	
	<!--달력-->
	<link rel="stylesheet" href="./components/BeatPicker.min.css" type="text/css">
	<script src="./components/BeatPicker.min.js"></script>

	<!--뷰 fancybox(2015버전)-->
	<link rel="stylesheet" href="./components/jquery.fancybox.css" type="text/css" media="screen">
	<script type="text/javascript" src="./components/jquery.fancybox.pack.js"></script>

	<link href="./components/font-awesome/css/font-awesome.css" rel="stylesheet">

	<!-- 시계 -->
	<link href="./components/bootstrap-timepicker.css" rel="stylesheet">
	<script src="./components/bootstrap-timepicker.js"></script>
	<!-- 달력 TIME처리 -->

	<!-- TOV.SG Update  -->
	<!-- Tag Input -->
	<script src="./components/jquery-tags-input/jquery.tagsinput.js"></script>
	<link rel="stylesheet" type="text/css" href="./components/jquery-tags-input/jquery.tagsinput.css" />
	<!-- Advance Form -->
	<script src="./components/advanced-form.js"></script>

	<!--icheck init -->
	<link href="./components/iCheck/skins/flat/blue.css" rel="stylesheet">
	<script src="./components/iCheck/jquery.icheck.js"></script>
	<script src="./components/iCheck/icheck-init.js"></script>

	<!-- Switch JS -->
	<link rel="stylesheet" href="./components/bootstrap-switch/bootstrap-switch.css" />
	<script src="./components/bootstrap-switch/bootstrap-switch.js"></script>
	<script src="./components/toggle-init.js"></script>

	<!-- Custom styles for this template -->
	<link href="./style/style.css" rel="stylesheet">
	<link href="./style/style-responsive.css" rel="stylweesheet">
	
	<!-- elTOV Singapore Custom Style & JS -->
	<link href="./style/tovsg_style.css" rel="stylesheet">
	<script src="./js/shop.js"></script>

</head>

<body> 
	<script>
  //시작과 동시에 로딩화면 보여주는 부분
  $(window).load(function(){
  	setTimeout(function(){
  		$('#loading').hide();
  	},10);
  });
</script>
<div id="loading" style="display: none;">
	<img id="loading-image" src="./asset/loading.gif" alt="Loading...">
</div>

<!--header start-->
<header class="header fixed-top clearfix">
	<!--로고부분 시작-->
	<div class="brand">

		<a href="http://jewel.tovms.com/stadmin/main/index.jsp" class="logo">
			<img src="./asset/logo_white.png">
		</a>

		<div class="sidebar-toggle-box">
			<div class="fa fa-bars"></div>
		</div>
	</div>
	<!--로고부분 종료-->

	<!--로고 옆 버튼 영역 시작-->
	<div class="nav notify-row" id="top_menu">
		<ul class="nav top-menu">
			<!-- notification dropdown start-->
			<li id="header_notification_bar">

			</li>
			<!-- notification dropdown end -->
		</ul>
	</div>
	<!--로고 옆 버튼 영역 종료-->
	<!--맨 우측 관리자 얼굴 영역 시작-->
	<div class="top-nav clearfix">
		<ul class="nav pull-right top-menu">
			<li class="dropdown">
				<div class="" style="width:200px;height:50px;line-height:45px;text-align:center;">
					<img src="http://jewel.tovms.com/zcommonfiles/branch/JEW//B0000000002_MAIN.png" width="29px" height="31px" style="position:relative; left:0px;">
					<span class="username" style="padding-left: 5px; font-weight:bold;">Jewel Changi Airport</span>
				</div>
			</li>

			<li class="dropdown" style="padding-left:10px;">
				<a data-toggle="dropdown" class="dropdown-toggle" href="http://jewel.tovms.com/stadmin/contents/store_info.jsp?store_id=35&amp;store_name=&amp;pageno=&amp;pagesize=20&amp;search_cate=&amp;search_floor=&amp;search_name=&amp;display_type=&amp;s_store_status=&amp;s_sort=#" style="height:50px;line-height:45px;">

					<img src="./asset/no_img.jpg" width="29px" height="31px" style="position:relative; left:18px;"><!--관리자 얼굴-->
					<span class="username" style="position:relative; right:10px; padding-left:30px;">Hello, John Doe</span>
					<b class="caret" style="position:relative; right:10px;"></b>
				</a>
				<ul class="dropdown-menu extended logout">
					<li><a href="http://jewel.tovms.com/stadmin/system/user_info.jsp?user_id=1"><i class=" fa fa-suitcase"></i><span id="id_profile_txt">Profiles</span></a></li>
					<li><a href="http://jewel.tovms.com/stadmin/include/logoutaction.jsp"><i class="fa fa-key"></i><span id="id_logout_txt">Logout</span></a></li>
				</ul>
			</li>
		</ul>
	</div>
	<!--맨 우측 관리자 얼굴 영역 종료-->
	<form name="frmpage1" action="http://jewel.tovms.com/stadmin/contents/store_info.jsp?store_id=35&amp;store_name=&amp;pageno=&amp;pagesize=20&amp;search_cate=&amp;search_floor=&amp;search_name=&amp;display_type=&amp;s_store_status=&amp;s_sort=#">
		<input type="hidden" name="n_pageno" value="1">
		<input type="hidden" name="popyn" value="Y">
	</form>

	<script>

		function setPageSubmit1(no){
			document.frmpage1.n_pageno.value = no;
			document.frmpage1.submit();
		}

		function setClickLang(lang){
			if(lang != ""){
				$(document).ready(function(){
					jQuery.ajax({
						type : "POST",
						url : "/stadmin/include/sessioncheck.jsp", 
						data : "lang_type=" + lang,

						success : function(){
							if(lang == "KOR"){
								$("#id_lang_txt").html("한국어");
							}else if(lang == "ENG"){
								$("#id_lang_txt").html("ENGLISH");
							}else if(lang == "CHN"){
								$("#id_lang_txt").html("中文");
							}else if(lang == "JPN"){
								$("#id_lang_txt").html("日本語");
							}

							setChangeLang(lang);
						},

						error : function(xhr, status, error){
							alert("Error.");
							return;
						}
					});
				});
			}
		}


		function setNoticeCreate(){
			$('#id_modal_notice_create').modal();
		}

		function setNoticeList(){
			$('#id_modal_notice_list').modal();
		}

	</script>
	<!--header end-->
</header>

<!--left start-->
<aside>
	<div id="sidebar" class="nav-collapse">
		<div class="leftside-navigation" tabindex="5000" style="overflow: hidden; outline: none;">
			<ul class="sidebar-menu" id="nav-accordion">
<!-- 				<li>
					<a href="#" class="">
						<i class="fa fa-bar-chart-o"></i>
						<span id="id_main_dash_txt">Dashboard</span>
					</a>
				</li> -->

<!-- 				<li class="sub-menu dcjq-parent-li">
					<a href="javascript:;" class="dcjq-parent">
						<i class="fa fa-cloud-upload"></i>
						<span class="id_main_upload_txt">Controlling Contents</span>
						<span class="dcjq-icon"></span></a>
						<ul class="sub" style="display: none;">
							<li class="">
								<a href="#"><span class="fa fa-caret-right"></span><span class="id_sub_contents_txt">Media Upload</span></a>
							</li>
							<li class="">
								<a href="#"><span class="fa fa-caret-right"></span>  <span class="id_sub_calendar_txt">Calendar</span></a>
							</li>
							<li class="">
								<a href="#"><span class="fa fa-caret-right"></span>  <span class="id_sub_schedule_txt">Schedule</span></a>
							</li>
							<li>
								<div style="width:100%;height:1px;border-bottom:1px solid rgba(255,255,255,0.06)"></div>
							</li>
							<li class="">
								<a href="#"><span class="fa fa-caret-right"></span>  <span class="id_sub_contents_txt">Gift Idea Upload</span></a>
							</li>
							<li class="">
								<a href="#"><span class="fa fa-caret-right"></span>  <span class="id_sub_contents_txt">Gift Idea Schedule</span>
								</a>
							</li>
						</ul>
					</li> -->

					<li class="sub-menu dcjq-parent-li">
						<a href="javascript:;" class="active dcjq-parent">
							<i class="fa fa-gear"></i>
							<span class="id_main_device_txt">Device</span>
							<span class="dcjq-icon"></span></a>
							<ul class="sub" style="display: block;">
<!-- 								<li class=""><a href="http://jewel.tovms.com/stadmin/system/kiosk_monitoring.jsp"><span class="fa fa-caret-right"></span>  <span class="id_sub_monitoring_txt">Monitoring</span></a></li>

	<li class=""><a href="http://jewel.tovms.com/stadmin/system/kiosk_list.jsp"><span class="fa fa-caret-right"></span>  <span class="id_sub_device_txt">Device</span></a></li> -->

	<li class="checking"><a href="./shop.php"><span class="fa fa-caret-right"></span>  <img src="./asset/shop.png" class="icon" />  <span class="id_sub_store_txt">Stores</span></a></li>

<!-- 								<li class=""><a href="http://jewel.tovms.com/stadmin/system/map_list.jsp"><span class="fa fa-caret-right"></span>  <span class="">Map</span></a></li>

								<li class=""><a href="http://jewel.tovms.com/stadmin/system/public_list.jsp"><span class="fa fa-caret-right"></span>  <span class="id_sub_public_txt">Facilities</span></a></li>
								<li class=""><a href="http://jewel.tovms.com/stadmin/system/stats_list.jsp"><span class="fa fa-caret-right"></span>  <span class="id_sub_stats_txt">Statistics</span></a></li>

								<li class=""><a href="http://jewel.tovms.com/stadmin/system/program_list.jsp"><span class="fa fa-caret-right"></span>  <span class="id_sub_program_txt">Program</span></a></li>
								<li class=""><a href="http://jewel.tovms.com/stadmin/system/user_list.jsp"><span class="fa fa-caret-right"></span>  <span class="id_sub_user_txt">User</span></a></li> -->

							</ul>
						</li>
					</ul>
				</div>
			</div>
		</aside>