<?php @include 'header.php';?>

<div id="container">

  <section id="main-content" class="">
    <section class="wrapper">

      <div class="col-sm-12 m-bot15">
        <section class="panel">
          <header class="panel-heading clearfix">
            <div class="col-sm-8" style="margin-top: 10px;">
              <strong>
                <span class="id_main_device_txt">Device</span>
                &nbsp;<span class="fa fa-angle-double-right" style="color:#996600;"></span>&nbsp;
                <span class="id_sub_store_txt">Store (List)</span>
              </strong>
            </div>
          </header>
        </section>
      </div>

      <div class="col-md-12">
        <section class="panel">
          <div class="panel-body">
            <div class="row" style=" height: 55px; padding: 10px;">
              <div class="col-sm-8"> 
                <div class="col-sm-7">
                  <input type="text" name="search_name" value="" placeholder="Search by Shop Name" class="form-control round-border input-lg" style="width:100%;">
                  <button type="button" class="btn btn-default search-inside-icon" onclick="setPageSearch();"><span id="id_button01_txt"><i class="fa fa-search " aria-hidden="true"></i></span></button>
                </div>
                <div class="col-sm-5">
                  <div class="flat-blue single-row" style="padding: 5px;">
                    <div class="checkbox" style="display: inline-block;">
                      <input type="checkbox" name="s_store_status" value="A">
                      <label> Only not loaded </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-2 col-sm-push-2 text-right">
               <nav class="menu">
                <input type="checkbox" href="#" class="menu-open" name="menu-open" id="menu-open" />
                <label class="menu-open-button" for="menu-open">
                  <span class="hamburger hamburger-1"></span>
                  <span class="hamburger hamburger-2"></span>
                  <span class="hamburger hamburger-3"></span>
                </label>

                <a href="#" class="menu-item" onclick="setCreateMove()">
                  <span class="tooltips" data-placement="top" data-toggle="tooltip" data-original-title="Add Store">
                    <i class="fa fa-plus" aria-hidden="true"></i> 
                  </span>
                </a>
                <a href="#" class="menu-item" onclick="setCreateMove()"><span class="tooltips" data-placement="top" data-toggle="tooltip" data-original-title="Preview Map"><i class="fa fa-map" aria-hidden="true"></i>  </span> </a>
                <a href="#" class="menu-item" onclick="getExcel(2)"><span class="tooltips" data-placement="top" data-toggle="tooltip" data-original-title="Export Excel">
                  <i class="fa fa-file-excel-o" aria-hidden="true"></i></span> 
                </a>
                <a href="#" class="menu-item" onclick="setCheckDel()"> 
                  <span class="tooltips" data-placement="top" data-toggle="tooltip" data-original-title="Delete Selected"> 
                    <i class="fa fa-trash-o" aria-hidden="true"></i>
                  </span>
                </a>
              </nav>
              <!-- Animation -->
              <svg xmlns="http://www.w3.org/2000/svg" version="1.1">
                <defs>
                  <filter id="shadowed-goo">
                    <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="10" />
                    <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo" />
                    <feGaussianBlur in="goo" stdDeviation="3" result="shadow" />
                    <feColorMatrix in="shadow" mode="matrix" values="0 0 0 0 0  0 0 0 0 0  0 0 0 0 0  0 0 0 1 -0.2" result="shadow" />
                    <feOffset in="shadow" dx="1" dy="1" result="shadow" />
                    <feComposite in2="shadow" in="goo" result="goo" />
                    <feComposite in2="goo" in="SourceGraphic" result="mix" />
                  </filter>
                  <filter id="goo">
                    <feGaussianBlur in="SourceGraphic" result="blur" stdDeviation="10" />
                    <feColorMatrix in="blur" mode="matrix" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 18 -7" result="goo" />
                    <feComposite in2="goo" in="SourceGraphic" result="mix" />
                  </filter>
                </defs>
              </svg>
            </div>
          </div>

          <div class="col-md-12 mt-3">
            <table class="table border">
              <thead>
                <tr>
                  <th>
                    <div class="flat-green"><div class="icheckbox_flat-blue" style="position: relative;">
                      <input type="checkbox" id="id_store_all" style="position: absolute; opacity: 0;">
                    </div>
                  </div>
                </th>
                <th>No</th>
                <th>Store Status
                  <a href="javascript:onClickSort('STORE_STATUS_ASC');"><i class="fa fa-sort" style="color:#dddddd;"></i></a>
                </th>   
                <th>Logo</th>
                <th>Name
                  <a href="javascript:onClickSort('NAME_ASC');"><i class="fa fa-sort" style="color:#dddddd;"></i></a>
                </th>
                <th>Category
                  <a href="javascript:onClickSort('CATE_ASC');"><i class="fa fa-sort" style="color:#dddddd;"></i></a>
                </th>
                <th>Floor
                  <a href="javascript:onClickSort('FLOOR_ASC');"><i class="fa fa-sort" style="color:#dddddd;"></i></a>
                </th>
                <th>Unit No.
                  <a href="javascript:onClickSort('NUMBER_ASC');"><i class="fa fa-sort" style="color:#dddddd;"></i></a>
                </th>
                <th>Added Date
                  <a href="javascript:onClickSort('REG_ASC');"><i class="fa fa-sort" style="color:#dddddd;"></i></a>
                </th>
                <th>Added By
                  <a href="javascript:onClickSort('REG_ASC');"><i class="fa fa-sort" style="color:#dddddd;"></i></a>
                </th>
                <th>Updated Date
                  <a href="javascript:onClickSort('UPDATE_ASC');"><i class="fa fa-sort" style="color:#dddddd;"></i></a>
                </th>
                <th>Updated By
                  <a href="javascript:onClickSort('UPDATE_ASC');"><i class="fa fa-sort" style="color:#dddddd;"></i></a>
                </th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <tr class="odd">
                <td>
                  <div class="flat-green code-check">
                    <div class="icheckbox_flat-blue" style="position: relative;">
                      <input id="id_store_check0" class="ck_box" type="checkbox" value="35">
                    </div>
                  </div>
                </td>
                <td>1</td>
                <td style="cursor: pointer;"><span class="label label-primary"> Loaded</span></td>
                <td>
                  <a href="http://jewel.tovms.com/zcommonfiles/store/JEW/S0000000035_THUM.jpg" data-fancybox-group="gallery" class="fancybox tooltips" data-placement="top" data-toggle="tooltip" data-original-title="">
                    <div>
                      <img src="http://jewel.tovms.com/zcommonfiles/store/JEW/S0000000035_THUM.jpg" class="img-thumb"/>
                    </div>
                  </a>
                </td>
                <td><a href="javascript:setPageMove('35');" class="blue_list">eng name</a></td>
                <td>Dine</td>
                <td>L2</td>
                <td>#38-24</td>
                <td>2018-11-14</td>
                <td>Manual</td>
                <td>2018-11-21</td>
                <td>John Doe</td>
                <td>
                  <div style="display:inline-block;">
                    <button type="button" class="btn btn-white btn-sm tooltips" onclick="edit()" style="float:left;margin-right:15px;" data-placement="top" data-toggle="tooltip" data-original-title="Export Excel"><span id=""><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Edit  </span></button>
                    <button type="button" class="btn btn-white btn-sm tooltips" onclick="setStoreDel('35')" style="float:left;margin-right:15px;" data-original-title="" title=""><span id=""><i class="fa fa-trash-o" aria-hidden="true"></i>  Delete</span></button>
                  </div>
                </td>
              </tr>
              <tr class="hidden-table" style="display:none">

              </tr>
              <tr>
                <td>
                  <div class="flat-green code-check">
                    <div class="icheckbox_flat-blue" style="position: relative;">
                      <input id="id_store_check0" class="ck_box" type="checkbox" value="35">
                    </div>
                  </div>
                </td>
                <td>1</td>
                <td style="cursor: pointer;"><span class="label label-danger" onclick="setDetailTable(this);"> <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>Not Loaded</span></td>
                <td>
                  <a href="http://jewel.tovms.com/zcommonfiles/store/JEW/S0000000035_THUM.jpg" data-fancybox-group="gallery" class="fancybox tooltips" data-placement="top" data-toggle="tooltip" data-original-title="">
                    <div>
                      <img src="http://jewel.tovms.com/zcommonfiles/store/JEW/S0000000035_THUM.jpg" class="img-thumb"/>
                    </div>
                  </a>
                </td>
                <td><a href="javascript:setPageMove('35');" class="blue_list">eng name 2</a></td>
                <td>Dine</td>
                <td>L2</td>
                <td>#38-24</td>
                <td>2018-11-14</td>
                <td>API</td>
                <td>2018-11-21</td>
                <td>John Doe</td>
                <td>
                  <div style="display:inline-block;">
                    <button type="button" class="btn btn-white btn-sm tooltips" onclick="edit()" style="float:left;margin-right:15px;" data-original-title="" title=""><span id=""><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Edit  </span></button>
                    <button type="button" class="btn btn-white btn-sm tooltips" onclick="setStoreDel('35')" style="float:left;margin-right:15px;" data-original-title="" title=""><span id=""><i class="fa fa-trash-o" aria-hidden="true"></i>  Delete</span></button>
                  </div>
                </td>
              </tr>
              <tr class="hidden-table" style="display:none">
                <td colspan="11">
                  <div style="padding-bottom:10px;padding-left:10px;">
                   Store Name (ENG):
                 </div>
                 <div style="padding-bottom:10px;padding-left:10px;">
                   Please address the following
                 </div>
                 <div style="padding-bottom:10px;padding-left:20px;color:red;">
                 </div>
               </td>
             </tr>
             <tr class="odd">
              <td>
                <div class="flat-green code-check">
                  <div class="icheckbox_flat-blue" style="position: relative;">
                    <input id="id_store_check0" class="ck_box" type="checkbox" value="35">
                  </div>
                </div>
              </td>
              <td>1</td>
              <td style="cursor: pointer;"><span class="label label-danger" onclick="setDetailTable(this);"> <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>  Not Loaded</span></td>
              <td>
                <a href="http://jewel.tovms.com/zcommonfiles/store/JEW/S0000000035_THUM.jpg" data-fancybox-group="gallery" class="fancybox tooltips" data-placement="top" data-toggle="tooltip" data-original-title="">
                  <div>
                    <img src="http://jewel.tovms.com/zcommonfiles/store/JEW/S0000000035_THUM.jpg" class="img-thumb"/>
                  </div>
                </a>
              </td>
              <td><a href="javascript:setPageMove('35');" class="blue_list">eng name 3</a></td>
              <td>Dine</td>
              <td>L2</td>
              <td>#38-24</td>
              <td>2018-11-14</td>
              <td>API</td>
              <td>2018-11-14</td>
              <td> - </td>
              <td>
                <div style="display:inline-block;">
                  <button type="button" class="btn btn-white btn-sm tooltips" onclick="edit()" style="float:left;margin-right:15px;" data-original-title="" title=""><span id=""><i class="fa fa-pencil-square-o" aria-hidden="true"></i>  Edit  </span></button>
                  <button type="button" class="btn btn-white btn-sm tooltips" onclick="setStoreDel('35')" style="float:left;margin-right:15px;" data-original-title="" title=""><span id=""><i class="fa fa-trash-o" aria-hidden="true"></i>  Delete</span></button>
                </div>
              </td>
            </tr>
            <tr class="hidden-table" style="display:none">
              <td colspan="11">
                <div style="padding-bottom:10px;padding-left:10px;">
                 Store Name (ENG):
               </div>
               <div style="padding-bottom:10px;padding-left:10px;">
                 Please address the following
               </div>
               <div style="padding-bottom:10px;padding-left:20px;color:red;">
               </div>
             </td>
           </tr>
         </tbody>
       </table>
     </div>
     <div class="col-md-2" style="text-align:left;">
      <span style="color:#999999;font-size:12px;">page</span> <strong>1</strong>/<strong>3</strong>
    </div>
    <div class="col-md-10" style="text-align:right;padding-bottom:7px;">
      <a href="#" class="paging_select">1</a>
      <a href="#" class="paging_none">2</a>
      <a href="#" class="paging_none">3</a>
      <a href="#" class="paging_none"> > </a> 
      <a href="#" class="paging_none">Last</a> 
    </div>
  </div>
</section>
</div>
</section>

</section>
</div>

<script type="text/javascript">
  function edit(){
   window.location.href='shop_edit.php';
 }

 function setPageSubmit(no){
  document.frmpage.pageno.value = no;
  document.frmpage.submit();
}

function setPageSearch(){
  document.frmsearch.submit();
}

function setPageMove(id){
  document.frmpage.store_id.value = id;
  document.frmpage.action = "store_info.jsp";
  document.frmpage.submit();
}

function setCreateMove(){
  document.frmpage.action = "store_write.jsp";
  document.frmpage.submit();
}

function setSizeChange(){
  var obj = document.getElementById("id_page_size");
  var index_value = obj.selectedIndex;
  var pagesize = obj.options[index_value].value;
  document.frmsearch.pagesize.value = pagesize;
  document.frmsearch.submit();
}

function onClickSort(p_sort){
  document.frmsearch.s_sort.value = p_sort;
  document.frmsearch.submit();

}

    function setDetailTable(obj){ // 숨겨진 테이블 세부 보기 함수
      $(obj).parent().parent().next().toggle(300);
    }

    //스타일 적용 부분
    $('.oddo').children().each(function(){$(this).css({'text-align':'center'});});
    $('.eve').children().each(function(){
      $(this).css({'text-align':'center','cursor':'pointer'});
      $(this).hover(function(){$(this).css('color','black')},function(){$(this).css('color','#767676')});
    });

    $('.fancybox').each(function(){    // fancybox(logo 이미지 팝업) 사용 부분
      $(this).fancybox({openEffect:'elastic',closeEffect:'elastic',helpers:{title:{type:'inside'}}});
    });

    $('.minimal input').iCheck({ //라디오를 그리는 함수
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue',
      increaseArea: '20%'
    });

    
    // 한개삭제.
    function setStoreDel(store_id){

      if(confirm("Are you sure you want to delete?")){
        document.frmdel.store_id.value = store_id;
        document.frmdel.mode.value = "DELETE";
        document.frmdel.submit();
      }
    }

     // 전체삭제.
     function setCheckDel(){
       var del_ids = "";
       $('.ck_box').each(function(){
         if($(this).is(":checked")){
           del_ids += $.trim($(this).val());
           del_ids += ",";
         }
       });

       del_ids = del_ids.substr(0,del_ids.length-1);

       if(del_ids.length == 0){
         alert("Please select contents.");
         return;
       }

       if(confirm("Are you sure you want to delete?")){
         document.frmdel.store_id.value = del_ids;
         document.frmdel.mode.value = "ALL_DELETE";
         document.frmdel.submit();
       }
     }

     function getExcel(brn_id){
      location.href="excel/store_excel.jsp?brn_id=" + brn_id;
    }
    
    jQuery(document).ready(function(){
      var i = 0;
      var j = 0;
      var store_id_list = ""; 
      var check_length = $('.code-check').length;

        //체크박스를 그리는 함수
        $('.flat-green input').iCheck({
          checkboxClass: 'icheckbox_flat-blue',
          radioClass: 'iradio_flat-blue'
        });

        //전체 체크박스 선택
        $('#id_store_all').on('ifChecked', function(event){
          $('.flat-green input').iCheck('check');
        });

        //전체 체크박스 해제
        $('#id_store_all').on('ifUnchecked', function(event){
          $('.flat-green input').iCheck('uncheck');
        });        
      });
    </script>

    <?php @include 'footer.php'?>