<?php @include 'header.php';?>

<section id="container">
  <!--main content start-->
  <section id="main-content" class="">
    <section class="wrapper">
      <div class="col-md-12">
        <section class="panel">
          <header class="panel-heading">
            <strong>
              Device
              <span class="fa fa-angle-double-right" style="color:#996600;"></span> 
              Store
              <span class="fa fa-angle-double-right" style="color:#996600;"></span> 
              Edit
            </strong>
          </header>

          <form name="frmwrite" method="post" action="#" class="form-horizontal bucket-form" enctype="multipart/form-data">
            <input type="hidden" name="mode" value="UPDATE">
            <input type="hidden" name="store_id" value="35">
            <input type="hidden" name="store_type" value="02">
            <input type="hidden" name="store_sect" value="">
            <input type="hidden" name="store_tag" value="">
            <input type="hidden" name="search_type" value="">
            <input type="hidden" name="display_type" value="">
            <input type="hidden" name="user_type" value="">
            <input type="hidden" name="store_servicetime" value="">
            <input type="hidden" name="store_status" value="">

            <input type="hidden" name="logo_file_id" value="1006">
            <input type="hidden" name="main_file_id" value="1005">
            <input type="hidden" name="thum_file_id" value="1007">
            <input type="hidden" name="logo_file_name" value="S0000000035_LOGO.jpg">
            <input type="hidden" name="main_file_name" value="S0000000035_MAIN.jpg">
            <input type="hidden" name="thum_file_name" value="S0000000035_THUM.jpg">

            <input type="hidden" name="map_file_id" value="">
            <input type="hidden" name="map_file_name" value="">

            <input type="hidden" name="store_ordertime" value="">
            <input type="hidden" name="store_breaktime" value="">
            <div class="panel-body">

              <!-- 로고파일 -->
              <div class="form-group">
                <label class="col-sm-2 control-label">
                 Store Logo : 
               </label>
               <div class="col-sm-4">
                <div style="display: inline-block">
                  <a href="http://jewel.tovms.com/zcommonfiles/store/JEW/S0000000035_LOGO.jpg" data-fancybox-group="gallery" class="fancybox">
                    <img src="./asset/S0000000035_THUM.jpg" style="max-width:100px; max-height:50px;" class="thumbnail">
                  </a>
                </div>
                <div style="display: inline-block; vertical-align: top;">
                  <span class="btn btn-primary btn-file">
                    Browse <input type="file" id="id_logo" name="logo" class="files2">
                  </span>
                  <span class="addon2"></span>
                </div>
                <div style="padding-top:2px;">
                 <i class="fa fa-certificate" aria-hidden="true"></i>
                 <span>
                  png/.jpg/.jpeg format (486x826)
                </span>
              </div>
            </div>


            <label class="col-sm-2 control-label"> Store Image : </label>
            <div class="col-sm-4">
              <div style="display: inline-block">
                <a href="./asset/S0000000035_MAIN.jpg" data-fancybox-group="gallery" class="fancybox">
                  <img src="./asset/S0000000035_MAIN.jpg" style="max-width:100px; max-height:50px;" class="thumbnail">
                </a>
              </div>
              <div  style="display: inline-block; vertical-align: top;">
                <span class="btn btn-primary btn-file">
                  Browse <input type="file" id="id_main" name="main" class="files1">
                </span>
                <span class="addon1"></span>
              </div>
              <div style="padding-top:2px;">
                <i class="fa fa-certificate" aria-hidden="true"></i>
                .png/.jpg/.jpeg format (486x826)
              </div>
            </div>
            <div class="text-center"> <span style="color:#FF6C60;"> * </span> Maximum upload file size 2MB. Allowed file type. <span style="color:#FF6C60;"> * </span> </div>
          </div>


          <!-- tab nav Start -->
          <div class="form-group">
            <section class="panel language-panel">
              <header class="panel-heading tab-bg-dark-navy-blue ">
                <ul class="nav nav-tabs">
                  <li class="active">
                    <a data-toggle="tab" href="#eng">English</a>
                  </li>
                  <li class="">
                    <a data-toggle="tab" href="#zh">Chinese</a>
                  </li>
                  <li class="">
                    <a data-toggle="tab" href="#kr">Korean</a>
                  </li>
                  <li class="">
                    <a data-toggle="tab" href="#indo">Bahasa Indonesia</a>
                  </li>
                  <li class="">
                    <a data-toggle="tab" href="#jpn">Japanese</a>
                  </li>
                </ul>
              </header>
              <div class="panel-body" style="padding: 15px 0px;">
                <div class="tab-content">
                  <div id="eng" class="tab-pane fade in active">
                    <div class="clearfix block">
                      <label class="col-sm-2 control-label" style="">Store Name : </label>
                      <div class="col-sm-10">
                        <input type="text" name="store_name_eng" id="id_store_name_eng" class="form-control" maxlength="20" placeholder="Store name (English)"/>
                        <div style="padding-top:5px; margin-bottom: 25px;">
                         <i class="fa fa-certificate" aria-hidden="true"></i>
                         <span style="font-size:11px;">Max 20 Words </span>
                       </div> 
                     </div>
                   </div>
                   <div class="clearfix block">
                    <label class="col-sm-2 control-label" style="">Store Description : </label>
                    <div class="col-sm-10">
                      <textarea name="store_desc_eng" id="id_store_desc_eng" cols="32" class="form-control" style="resize:none;height:100px;" placeholder="Description (English)..." class="form-control" maxlength="255"></textarea>
                      <div style="padding-top:5px;">
                       <i class="fa fa-certificate" aria-hidden="true"></i>     
                       <span style="font-size:11px;">Max 255 Words. (<span class="chars">255</span>) </span><br>
                     </div>
                   </div>
                 </div>
               </div>
               <div id="zh" class="tab-pane fade">
                <div class="block"> 
                  <label class="col-sm-2 control-label" style=""> Store Name : </label>
                  <div class="col-sm-10">
                    <input type="text" name="store_name_chn" id="id_store_name_eng" class="form-control"  maxlength="20" placeholder="Store name (Chinese)"/>
                    <p style="padding-top:5px; margin-bottom: 25px;">
                     <i class="fa fa-certificate" aria-hidden="true"></i> 
                     <span style="font-size:11px;">Max 20 Words</span>
                   </p>
                 </div>
               </div>
               <div class="block"> 
                <label class="col-sm-2 control-label" style="">Store Description : </label>
                <div class="col-sm-10">
                  <textarea name="store_desc_chn" id="id_store_desc_chn" cols="32" class="form-control" style="resize:none;height:100px;" placeholder="Description (Chinese)..."></textarea>
                  <div style="padding-top:5px;">
                   <i class="fa fa-certificate" aria-hidden="true"></i>     
                   <span style="font-size:11px;">Max 255 Words. (<span class="chars">255</span>) </span><br>
                 </div>
               </div>
             </div>
           </div>

           <div id="kr" class="tab-pane">
            <div class="block">
              <label class="col-sm-2 control-label" style=""> Store Name : </label>
              <div class="col-sm-10">
                <input type="text" name="store_name_kor" id="id_store_name_eng" class="form-control" maxlength="20" placeholder="Store name (Korean)"/>
                <div style="padding-top:5px; margin-bottom: 25px;">
                 <i class="fa fa-certificate" aria-hidden="true"></i> 
                 <span style="font-size:11px;">Max 20 Words.</span>
               </div>
             </div>
           </div>
           <div class="block">
            <label class="col-sm-2 control-label" style=""> Store Description : </label>
            <div class="col-sm-10">
              <textarea name="store_desc_kor" id="id_store_desc_kor" cols="32" class="form-control" style="resize:none;height:100px;" placeholder="Description (Korean)..."></textarea>
              <div style="padding-top:5px;">
               <i class="fa fa-certificate" aria-hidden="true"></i>     
               <span style="font-size:11px;">Max 255 Words. (<span class="chars">255</span>) </span><br>
             </div>
           </div>
         </div>
       </div>

       <div id="indo" class="tab-pane fade">
        <div class="block">
          <label class="col-sm-2 control-label" style=""> Store Name : </label>
          <div class="col-sm-10">
            <input type="text" name="store_name_idn" id="id_store_name_eng" class="form-control" maxlength="20" placeholder="Store name (Bahasa Indonesia)"/>
            <div style="padding-top:5px; margin-bottom: 25px;">
             <i class="fa fa-certificate" aria-hidden="true"></i> 
             <span style="font-size:11px;">Max 20 Words.</span>
           </div>
         </div>
       </div>
       <div class="block">
        <label class="col-sm-2 control-label" style=""> Store Description : </label>
        <div class="col-sm-10">
          <textarea name="store_desc_idn" id="id_store_desc_idn" cols="32" class="form-control" style="resize:none;height:100px;" placeholder="Description (Bahasa Indonesia)... "></textarea>
          <div style="padding-top:5px;">
           <i class="fa fa-certificate" aria-hidden="true"></i>     
           <span style="font-size:11px;">Max 255 Words. (<span class="chars">255</span>) </span><br>
         </div>
       </div>
     </div>
   </div>

   <div id="jpn" class="tab-pane fade">
    <div class="block">
      <label class="col-sm-2 control-label" style="">Store Name : </label>
      <div class="col-sm-10">
        <input type="text" name="store_name_jpn" id="id_store_name_eng" class="form-control" maxlength="20" placeholder="Store name (Japanese)"/>
        <div style="padding-top:5px; margin-bottom: 25px;">
         <i class="fa fa-certificate" aria-hidden="true"></i> 
         <span style="font-size:11px;">Max 20 Words.</span>
       </div>
     </div>
   </div>
   <div class="block">
    <label class="col-sm-2 control-label" style="">Store Description : </label>
    <div class="col-sm-10">
      <textarea name="store_desc_jpn" id="id_store_name_eng" cols="32" class="form-control" style="resize:none;height:100px;" placeholder="Description (Japanese)..."></textarea>
      <div style="padding-top:5px;">
       <i class="fa fa-certificate" aria-hidden="true"></i>     
       <span style="font-size:11px;">Max 255 Words. (<span class="chars">255</span>) </span><br>
     </div> 
   </div>
 </div>

</div>
</div>
</div>
</section>
</div>
<!--tab nav End-->

<div class="form-group">
  <div class="clearfix" style="margin-bottom: 25px;">
    <label class="col-xs-2 control-label"> Unit number : </label>
    <div class="col-xs-4">
      <input type="text" name="store_number" id="a5" size="25" value="" placeholder="eg. (#38-24)" class="form-control form-status" style="width:100%">
    </div>
  </div>
  <div class="clearfix">
    <label class="col-sm-2 control-label" style=""> Phone Number : </label>
    <div class="col-sm-4">
      <input type="text" name="store_phone" size="25" value="" placeholder="eg. (+65 1234567)" class="form-control form-status" style="width:100%">
    </div>
  </div>
</div>

<!-- Category -->
<div class="form-group">
  <label class="col-sm-2 control-label" style=""> Category : </label>
  <div class="col-xs-4">
    <select name="cate_code" class="form-control form-status" aria-controls="dynamic-table" style="cursor:pointer" id="id_cate_code">
      <option value="">Main category</option>
      <option value="S01">  Jewellery and Watches</option>
      <option value="S02">  Beauty and Wellness</option>
      <option value="S03">  Fashion</option>
      <option value="S04">  Handbags, Shoes and Accessories</option>
      <option value="S05">  Sports</option>
      <option value="S06">  Lifestyle and Gifts</option>
      <option value="S07">  Children and Maternity</option>
      <option value="S08">  Optical</option>
      <option value="S09">  Confectionery &amp; Deli</option>
      <option value="S10" selected="">  Dine</option>
      <option value="S11">  Supermarket</option>
      <option value="S12">  Cinema</option>
      <option value="S13">  General Services</option>
      <option value="S14">  Local Gifts</option>
      <option value="S15">  Extended Hours</option>
      <option value="S16">  24 Hours</option>
    </select>
  </div>
  <label class="col-xs-2 control-label" style=""> Sub-Category : </label>
  <div class="col-xs-2">
    <select name="subcate_code" class="form-control form-status" aria-controls="dynamic-table" style="cursor:pointer" id="id_subcate_code">
      <option value="S100">Restaurant</option><option value="S101">Fast Food</option><option value="S102" selected="">Cafe</option><option value="S103">Quick Bites</option><option value="S104">Food Court</option>
    </select>
  </div>

</div>

<div class="form-group">
  <label class=" col-md-2 control-label">Tags : </label>
  <div class="col-md-10">
    <input id="tags_1" type="text" class="tags" value="php,ios,javascript,ruby,android,kindle" />
  </div>
</div>

<div class="form-group">
  <div class="clearfix">
    <label class="col-xs-2 control-label"> QR Code URL : </label>
    <div class="col-xs-4">
      <input type="text" name="qr_url" id="a5" size="25" value="" placeholder="http://wwww.jewel.com.sg" class="form-control form-status" style="width:100%">
    </div>
    <label class="col-xs-2 control-label"> API Node : </label>
    <div class="col-xs-2">
      <input type="text" name="api_url" id="" size="25" value="1-kdk237c44" readonly class="form-control form-status" style="width:100%">
    </div>
  </div>
</div>

<div class="form-group">
  <div id="operation-hour">
    <label class="col-sm-2 control-label"> Operation Hour : </label>
    <label class="col-sm-1 control-label" id="id_time_label1" style="text-align:left;width:60px !important;">Open:</label>
    <div class="col-sm-2 select-time">
      <div class="input-group bootstrap-timepicker">
        <input type="text" id="id_sch_time1" value="00:00" class="form-control timepicker-default" readonly="">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
        </span>
      </div>
    </div>

    <label class="col-sm-1 control-label" id="id_time_label2" style="text-align:left;width:60px !important;">Close:</label>
    <div class="col-sm-2 select-time">
      <div class="input-group bootstrap-timepicker">
        <input type="text" id="id_sch_time2" value="23:59" class="form-control timepicker-default" readonly="">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
        </span>
      </div>
    </div>

    <div class="col-sm-3">
      <div class="flat-blue single-row">
        <div class="checkbox ">
          <input type="checkbox" id="id_sch_time3">
          <label>24 Hours</label>
        </div>
      </div>
    </div>
  </div>

  <div class="col-sm-12 clearfix text-center" style="margin-top: 15px;"> 
    <a data-show="#switch-components" id="show-date-advance" style="text-decoration: underline;"> Show Advance Options <i class="fa fa-chevron-up" aria-hidden="true"></i></a>
  </div>

  <div class="col-xs-12" style="padding: 0px;">
   <div id="switch-components" style="display: none">

    <div class="clearfix">
      <div class="col-sm-2 text-right">
        Advance Operation Hour:
      </div>
      <div class="col-sm-2 switch-select">
        <input type="checkbox" checked data-on="primary" checked data-on-label="Mon" data-off-label="Close">    
      </div>
      <label class="col-sm-1 control-label" id="id_time_label1" style="text-align:left;width:60px !important;">Open:</label>
      <div class="col-sm-2 select-time">
        <div class="input-group bootstrap-timepicker">
          <input type="text" value="00:00" class="form-control timepicker-default" readonly="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
          </span>
        </div>
      </div>
      <label class="col-sm-1 control-label" id="id_time_label2" style="text-align:left;width:60px !important;">Close:</label>
      <div class="col-sm-2 select-time">
        <div class="input-group bootstrap-timepicker">
          <input type="text" value="23:59" class="form-control timepicker-default" readonly="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
          </span>
        </div>
      </div>  
      <div class="col-sm-2">
        <div class="flat-blue single-row">
          <div class="checkbox 24hours">
            <input type="checkbox">
            <label>24 Hours</label>
          </div>
        </div>
      </div>  
    </div>

    <div class="clearfix">
      <div class="col-sm-2">
      </div>
      <div class="col-sm-2 switch-select">
        <input type="checkbox" checked data-on="primary" checked data-on-label="Tue" data-off-label="Close">
      </div>
      <label class="col-sm-1 control-label" id="id_time_label1" style="text-align:left;width:60px !important;">Open:</label>
      <div class="col-sm-2 select-time">
        <div class="input-group bootstrap-timepicker">
          <input type="text" value="00:00" class="form-control timepicker-default" readonly="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
          </span>
        </div>
      </div>
      <label class="col-sm-1 control-label" id="id_time_label2" style="text-align:left;width:60px !important;">Close:</label>
      <div class="col-sm-2 select-time">
        <div class="input-group bootstrap-timepicker">
          <input type="text" value="23:59" class="form-control timepicker-default" readonly="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
          </span>
        </div>
      </div>  
      <div class="col-sm-2">
        <div class="flat-blue single-row">
          <div class="checkbox 24hours">
            <input type="checkbox">
            <label>24 Hours</label>
          </div>
        </div>
      </div>    
    </div>

    <div class="clearfix">
      <div class="col-sm-2">
      </div>
      <div class="col-sm-2 switch-select">
        <input type="checkbox" checked data-on="primary" checked data-on-label="Wed" data-off-label="Close">
      </div>
      <label class="col-sm-1 control-label" id="id_time_label1" style="text-align:left;width:60px !important;">Open:</label>
      <div class="col-sm-2 select-time">
        <div class="input-group bootstrap-timepicker">
          <input type="text" value="00:00" class="form-control timepicker-default" readonly="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
          </span>
        </div>
      </div>
      <label class="col-sm-1 control-label" id="id_time_label2" style="text-align:left;width:60px !important;">Close:</label>
      <div class="col-sm-2 select-time">
        <div class="input-group bootstrap-timepicker">
          <input type="text" value="23:59" class="form-control timepicker-default" readonly="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
          </span>
        </div>
      </div> 
      <div class="col-sm-2">
        <div class="flat-blue single-row">
          <div class="checkbox 24hours">
            <input type="checkbox">
            <label>24 Hours</label>
          </div>
        </div>
      </div>     
    </div>

    <div class="clearfix">
      <div class="col-sm-2">
      </div>
      <div class="col-sm-2 switch-select">
        <input type="checkbox" checked data-on="primary" checked data-on-label="Thurs" data-off-label="Close">
      </div>
      <label class="col-sm-1 control-label" id="id_time_label1" style="text-align:left;width:60px !important;">Open:</label>
      <div class="col-sm-2 select-time">
        <div class="input-group bootstrap-timepicker">
          <input type="text" value="00:00" class="form-control timepicker-default" readonly="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
          </span>
        </div>
      </div>
      <label class="col-sm-1 control-label" id="id_time_label2" style="text-align:left;width:60px !important;">Close:</label>
      <div class="col-sm-2 select-time">
        <div class="input-group bootstrap-timepicker">
          <input type="text" value="23:59" class="form-control timepicker-default" readonly="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
          </span>
        </div>
      </div>   
      <div class="col-sm-2" >
        <div class="flat-blue single-row">
          <div class="checkbox 24horus">
            <input type="checkbox">
            <label>24 Hours</label>
          </div>
        </div>
      </div>   
    </div>    

    <div class="clearfix">
      <div class="col-sm-2">
      </div>
      <div class="col-sm-2 switch-select">
        <input type="checkbox" checked data-on="primary" checked data-on-label="Fri" data-off-label="Close">
      </div>
      <label class="col-sm-1 control-label" id="id_time_label1" style="text-align:left;width:60px !important;">Open:</label>
      <div class="col-sm-2 select-time">
        <div class="input-group bootstrap-timepicker">
          <input type="text" value="00:00" class="form-control timepicker-default" readonly="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
          </span>
        </div>
      </div>
      <label class="col-sm-1 control-label" id="id_time_label2" style="text-align:left;width:60px !important;">Close:</label>
      <div class="col-sm-2 select-time">
        <div class="input-group bootstrap-timepicker">
          <input type="text" value="23:59" class="form-control timepicker-default" readonly="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
          </span>
        </div>
      </div>    
      <div class="col-sm-2">
        <div class="flat-blue single-row">
          <div class="checkbox 24hours">
            <input type="checkbox">
            <label>24 Hours</label>
          </div>
        </div>
      </div>  
    </div>    

    <div class="clearfix">
      <div class="col-sm-2">
      </div>
      <div class="col-sm-2 switch-select">
        <input type="checkbox" checked data-on="danger" checked data-on-label="Sat" data-off-label="Close">
      </div>
      <label class="col-sm-1 control-label" id="id_time_label1" style="text-align:left;width:60px !important;">Open:</label>
      <div class="col-sm-2 select-time">
        <div class="input-group bootstrap-timepicker">
          <input type="text" value="00:00" class="form-control timepicker-default" readonly="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
          </span>
        </div>
      </div>
      <label class="col-sm-1 control-label" id="id_time_label2" style="text-align:left;width:60px !important;">Close:</label>
      <div class="col-sm-2 select-time">
        <div class="input-group bootstrap-timepicker">
          <input type="text" value="23:59" class="form-control timepicker-default" readonly="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
          </span>
        </div>
      </div>    
      <div class="col-sm-2">
        <div class="flat-blue single-row">
          <div class="checkbox 24hours">
            <input type="checkbox">
            <label>24 Hours</label>
          </div>
        </div>
      </div>  
    </div>    

    <div class="clearfix">
      <div class="col-sm-2">
      </div>
      <div class="col-sm-2  switch-select">
        <input type="checkbox" checked data-on="danger" checked data-on-label="Sun" data-off-label="Close">
      </div>
      <label class="col-sm-1 control-label" id="id_time_label1" style="text-align:left;width:60px !important;">Open:</label>
      <div class="col-sm-2 select-time">
        <div class="input-group bootstrap-timepicker">
          <input type="text" value="00:00" class="form-control timepicker-default" readonly="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
          </span>
        </div>
      </div>
      <label class="col-sm-1 control-label" id="id_time_label2" style="text-align:left;width:60px !important;">Close:</label>
      <div class="col-sm-2 select-time">
        <div class="input-group bootstrap-timepicker">
          <input type="text" value="23:59" class="form-control timepicker-default" readonly="">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button"><i class="fa fa-clock-o"></i></button>
          </span>
        </div>
      </div>   
      <div class="col-sm-2">
        <div class="flat-blue single-row">
          <div class="checkbox ">
            <input type="checkbox" id="id_sch_time3">
            <label>24 Hours</label>
          </div>
        </div>
      </div>   
    </div>   

  </div>

</div>

</div>


<div class="form-group">
  <label class="col-sm-2 control-label" style=""> Target Group : </label>
  <div class="col-sm-10">
    <div class="flat-blue single-row">
      <span style="display:inline-block">
        <div class="checkbox">
          <input type="checkbox" name="tmp_user_type_2" value="MS">
          <label> Male (Below 18 years)</label>
        </div>
      </span>   
      <span style="display:inline-block">
        <div class="checkbox">
          <input type="checkbox" name="tmp_user_type_2" value="MS">
          <label> Male (Above 18 years)</label>
        </div>
      </span>      
      <span style="display:inline-block">
        <div class="checkbox">
          <input type="checkbox" name="tmp_user_type_3" value="WY" checked="">
          <label> Female (Below 18 years)</label>
        </div>
      </span>      
      <span style="display:inline-block">
        <div class="checkbox">
          <input type="checkbox" name="tmp_user_type_4" value="WS" checked="">
          <label> Female (Above 18 years)</label>
        </div>
      </span>      
      <span style="display:inline-block">
        <div class="checkbox">
          <input type="checkbox" name="tmp_user_type_5" value="UY">
          <label> Unisex (Below 18 Years)</label>
        </div>
      </span>      
      <span style="display:inline-block">
        <div class="checkbox">
          <input type="checkbox" name="tmp_user_type_6" value="US">
          <label> Unisex (Above 18 Years)</label>
        </div>
      </span>            
    </div>
  </div>
</div>


<div class="form-group">
  <label class="col-sm-2 control-label" style=""> 
    <a data-original-title="Icon" data-content="Icons show on the store list and store pop up." data-placement="top" data-trigger="hover" class="popovers">  <i class="fa fa-question-circle" aria-hidden="true"></i> Icon :</a>
  </label>
  <div class="col-sm-10">
    <div class="flat-blue single-row">
      <div class="radio">
        <span style="display:inline-block;padding-right:43px;">
          <div class="checkbox">
            <input type="checkbox" name="tmp_store_type01" value="01">
            <label> Voucher</label>
          </div>
        </span>   
        <span style="display: inline-block;padding-right:32px;">
          <div class="checkbox" style="position: relative;">
            <input type="checkbox" name="tmp_store_type02" value="02" checked="">
            <label> e-Voucher</label>
          </div>
        </span>   
        <span style="display: inline-block;padding-right:2px;">
          <div class="checkbox" style="position: relative;">
            <input type="checkbox" name="tmp_store_type03" value="03">
            <label> Family Restaurant</label>
          </div>
        </span>   
        <span style="display: inline-block">
          <div class="checkbox" style="position: relative;">
            <input type="checkbox" name="tmp_store_type04" value="04">
            <label> Halal</label>
          </div>
        </span>
      </div>
    </div>
  </div>
</div>

<div class="form-group">
  <label class="col-sm-2 control-label" style=""> Display/Hide Store : </label>
  <div class="col-sm-4">
    <div class="input-group">
      <div style="display:inline-block;margin-right:15px;">
        <select id="id_display_type" name="tmp_display_type" class="form-control" aria-controls="dynamic-table" style="cursor:pointer">
          <option value="SHOW" selected=""> Show this store on map and shop list. (e.g. Business as usual) </option>
          <option value="LIST"> Show this store on shop list, hide on the map. (e.g. Coming soon) </option>
          <option value="MAP"> Hide this store on shop list, show on map. (e.g. Under Renovation) </option>
          <option value="HIDE"> Hide this store on map and shop list. (e.g. Deactivated) </option>
        </select>
      </div>                
    </div>
  </div>
</div>



<div class="form-group" id="default-map">

  <div class="map-top clearfix">
    <div class="col-sm-2 text-right"> 
      <label class="control-label shop-title"> Store Map: </label>
      <div> 
        <input type="text" name="store-name" value="Main Store" class="form-control form-status" style="display: none;"> 
      </div>
    </div>
    <div class="col-sm-2">
      <label class="control-label" > 
        <a data-original-title="Enterence Position" data-content="Pin point location for wayfinding. Select the store enterence on the map." data-placement="top" data-trigger="hover" class="popovers"> 
          <span style="color: #01B0F0;"> 
            <i class="fa fa-question-circle" aria-hidden="true"></i>
            Enterence Position : 
          </span>
        </a> 
      </label>
      <div>
        <input type="number" name="pos_help_x" id="id_pos_help_x" value="1620" class="form-control form-status" style="width:40%;display:inline-block">
        x 
        <input type="number" name="pos_help_y" id="id_pos_help_y" value="880" class="form-control form-status" style="width:40%;display:inline-block">
      </div>
    </div>

    <div class="col-sm-2">
      <label class="control-label"> 
        <a data-original-title="Store Position" data-content="Pin point location for store. Your pin point is the center of the store name." data-placement="top" data-trigger="hover" class="popovers"> 
          <span style="color: #FF0000;"  > 
            <i class="fa fa-question-circle" aria-hidden="true"></i>
          Store Position : </span>
        </a> 
      </label>
      <div>
        <input type="number" name="pos_x" id="id_pos_x" value="1608" class="form-control form-status" style="width:40%;display:inline-block">
        x 
        <input type="number" name="pos_y" id="id_pos_y" value="744" class="form-control form-status" style="width:40%;display:inline-block">
      </div>
    </div>

    <div class="col-sm-2"> 
      <label class="control-label" style=""> Font size : </label>
      <div class="">
        <input type="number" name="font_size" id="id_font_size" size="25" value="6" min="1" max="40" class="form-control form-status" style="width:100%">
      </div>
    </div>

    <div class="col-sm-2"> 
      <label class="control-label"> Floor : </label>
      <div class="input-group" style="width: 100%;">
        <select name="floor_code" id="id_floor_code" class="form-control form-status" aria-controls="dynamic-table" style="cursor:pointer" onchange="setFloorChange();">
          <option value="B5">  B5</option>
          <option value="B4">  B4</option>
          <option value="B3">  B3</option>
          <option value="B2">  B2</option>
          <option value="B2M">  B2M</option>
          <option value="B1">  B1</option>
          <option value="B1M">  B1M</option>
          <option value="L1">  L1</option>
          <option value="L2" selected="">  L2</option>
          <option value="L3">  L3</option>
          <option value="L4">  L4</option>
          <option value="L5">  L5</option>
        </select>
      </div> 
    </div>

    <div class="col-sm-2 delete-floor"> </div>

  </div>


  <div class="map-bottom clearfix">
    <div class="col-sm-2"> </div>
    <div class="col-sm-10">
      <div style="width:100%;">
        <table id="dataTalbe" class="table table-striped" style="width:985px;height:565px;border:1px solid #DDDDDD; margin-top: 25px;">
          <tbody><tr>
            <td>
              <iframe name="iframe_map" class="iframe_map_target" id="iframe_map" src="./extra/mapstore.html" style="width:985px;height:565px; margin: auto" frameborder="0" marginheight="0" marginwidth="0"></iframe>
              <span class="fa fa-info-circle" style="color:#990033;">Click on map for dropping point.</span> 
            </td>
          </tr>
        </tbody></table>
      </div>
    </div>
  </div>
</div>

<div id="more-map" style="display: none;">

  <section class="panel">
    <header class="panel-heading">
      Multiple Shops Location
      <span class="tools pull-right">
        <a href="javascript:;" class="fa fa-chevron-down"></a>
      </span>
    </header>
    <div class="panel-body" style="overflow: hidden; display: block;">
      <div id="lastchild"></div>
    </div>
  </section>

</div>

<div class="text-center" style="margin: 25px 0px;">
  <button type="button" class="btn btn-secondary" data-container="body" data-toggle="popover" data-placement="top" data-content="Are you sure add new shop? <a onclick='confirm_addfloor(this);'> <u style='color: #1ea500;'>Yes</u> </a> &nbsp;<a onclick='cancel_addFloor(this);'> <u style='color: #FF0000;'> No </u> </a> " onclick="add_store(this)">
    Add More Store Location
  </button>
</div>

<div class="form-group">
  <div class="col-sm-2"> <button type="button" class="btn btn-danger" onclick="setPageDelete()"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button> </div>
  <div class="col-sm-10" style="text-align:right;">
    <button type="button" class="btn btn-primary" onclick="setCreateMove()" style="margin-right:15px; background-color: #ff82a7;" data-original-title="" title=""><span id=""><i class="fa fa-map" aria-hidden="true"></i>  Map Preview</span></button>
    <button type="button" class="btn btn-primary" onclick="setValueCheck()"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
    <button type="button" class="btn btn-black" data-toggle="modal" data-target="#exampleModal"> Cancel </button>
  </div>
</div>

</div>
</form>
</section>
</div>

</section>
</section>
<!--main content end-->
</section>


<div id="save"> 
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body text-center">
          <i class="fa fa-info-circle  fa-5x" aria-hidden="true" style="color: #01B0F0;"></i> 
          <h3>Do you want to leave this page? <br> </h3>
          <h4>Changes you made may not be saved. </h4>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary  btn-md" data-dismiss="modal" onclick="go_shop();"> Leave </button>
          <button type="button" class="btn btn-primary  btn-md" data-dismiss="modal"> Stay </button>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="delete_store"> 
  <div class="modal fade" id="delete_store_popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-body text-center">
          <i class="fa fa-info-circle  fa-5x" aria-hidden="true" style="color: #01B0F0;"></i>
          <p class="store_delete_id d-none"> </p> 
          <h3>Do you want delete this store? <br> </h3>
          <h4>Changes cannot be undo. </h4>
        </div>
        <div class="modal-footer text-right">
          <button type="button" class="btn btn-secondary btn-md" onclick="delete_store_popup()" > Yes </button>
          <button type="button" class="btn btn-primary  btn-md" data-dismiss="modal"> No </button>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function go_shop(){
   window.location.href='shop.php';
 };
</script>


<?php @include 'footer.php'?>