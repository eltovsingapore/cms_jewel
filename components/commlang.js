//상단 프로필
var top_profile_kor = "프로필";
var top_profile_eng = "Profiles";
var top_profile_chn = "轮廓";
var top_profile_jpn = "プロフィール";

//상단 로그아웃
var top_logout_kor = "로그아웃";
var top_logout_eng = "Logout";
var top_logout_chn = "登出";
var top_logout_jpn = "ログアウト";

//상단 공지사항
var top_notice_kor = "공지사항";
var top_notice_eng = "Notifications";
var top_notice_chn = "通知";
var top_notice_jpn = "お知らせ";

//좌측 메인 메뉴
var left_main_dash_kor = "분석도구";
var left_main_upload_kor = "업로드";
var left_main_device_kor = "디바이스";
var left_main_dash_eng = "Dashboard";
var left_main_upload_eng = "Upload";
var left_main_device_eng = "Device";
var left_main_dash_chn = "仪表板";
var left_main_upload_chn = "上传";
var left_main_device_chn = "设备";
var left_main_dash_jpn = "ダッシュボード";
var left_main_upload_jpn = "アップロード";
var left_main_device_jpn = "デバイス";

//좌측 서브 메뉴
var left_sub_contents_kor = "컨텐츠";
var left_sub_schedule_kor = "스케줄";
var left_sub_calendar_kor = "달력";
var left_sub_monitoring_kor = "모니터링";
var left_sub_device_kor = "디바이스";
var left_sub_event_kor = "이벤트";
var left_sub_store_kor = "매장";
var left_sub_public_kor = "공영시설";
var left_sub_stats_kor = "통계";
var left_sub_log_kor = "로그";
var left_sub_program_kor = "프로그램";
var left_sub_user_kor = "관리자";

var left_sub_contents_eng = "Contents";
var left_sub_schedule_eng = "Schedule";
var left_sub_calendar_eng = "Calendar";
var left_sub_monitoring_eng = "Monitoring";
var left_sub_device_eng = "Device";
var left_sub_event_eng = "Event";
var left_sub_store_eng = "Store";
var left_sub_public_eng = "Public";
var left_sub_stats_eng = "Statistics";
var left_sub_log_eng = "Logs";
var left_sub_program_eng = "Program";
var left_sub_user_eng = "User";

var left_sub_contents_chn = "内容";
var left_sub_schedule_chn = "时间表";
var left_sub_calendar_chn = "日历";
var left_sub_monitoring_chn = "监控";
var left_sub_device_chn = "设备";
var left_sub_event_chn = "事件";
var left_sub_store_chn = "商店";
var left_sub_public_chn = "上市";
var left_sub_stats_chn = "统计";
var left_sub_log_chn = "登陆";
var left_sub_program_chn = "程序";
var left_sub_user_chn = "用户";

var left_sub_contents_jpn = "内容物";
var left_sub_schedule_jpn = "スケジュール";
var left_sub_calendar_jpn = "カレンダー";
var left_sub_monitoring_jpn = "モニタリング";
var left_sub_device_jpn = "デバイス";
var left_sub_event_jpn = "イベント";
var left_sub_store_jpn = "格納";
var left_sub_public_jpn = "パブリック";
var left_sub_stats_jpn = "統計";
var left_sub_log_jpn = "ログ";
var left_sub_program_jpn = "プログラム";
var left_sub_user_jpn = "ユーザー";

//우측 메뉴
var right_title01_kor = "로그/관리자 현황";
var right_title02_kor = "관리자 로그 현황";
var right_title03_kor = "관리자 현황";

var right_title01_eng = "Log/User Status";
var right_title02_eng = "User logs status";
var right_title03_eng = "User status";

var right_title01_chn = "登陆/用户 状态";
var right_title02_chn = "用户 登陆 状态";
var right_title03_chn = "用户 状态";

var right_title01_jpn = "ログ/ユーザー 状態";
var right_title02_jpn = "ユーザー ログ 状態";
var right_title03_jpn = "ユーザー 状態";

//본문 텍스트
var body_text01_kor = "검색";
var body_text02_kor = "목록";
var body_text03_kor = "정보";
var body_text04_kor = "등록";
var body_text05_kor = "터치 디바이스";
var body_text06_kor = "광고 디바이스";
var body_text07_kor = "버전 업데이트";
var body_text08_kor = "파일 첨부";
var body_text09_kor = "파일 리스트";
var body_text10_kor = "전체";
var body_text11_kor = "이미지";
var body_text12_kor = "영상";
var body_text13_kor = "모두적용";

var body_text01_eng = "Search";
var body_text02_eng = "List";
var body_text03_eng = "Information";
var body_text04_eng = "Create";
var body_text05_eng = "Touch device";
var body_text06_eng = "Ad device";
var body_text07_eng = "Version update";
var body_text08_eng = "File Attachments";
var body_text09_eng = "File List";
var body_text10_eng = "ALL";
var body_text11_eng = "IMG";
var body_text12_eng = "MOV";
var body_text13_eng = "APPLY ALL";

var body_text01_chn = "搜索";
var body_text02_chn = "名单";
var body_text03_chn = "信息";
var body_text04_chn = "创建";
var body_text05_chn = "触摸设备";
var body_text06_chn = "广告 设备";
var body_text07_chn = "版本更新";
var body_text08_chn = "附加文件";
var body_text09_chn = "列表文件";
var body_text10_chn = "整体";
var body_text11_chn = "图片";
var body_text12_chn = "图片";
var body_text13_chn = "所有应用";


var body_text01_jpn = "サーチ";
var body_text02_jpn = "リスト";
var body_text03_jpn = "情報";
var body_text04_jpn = "作り出します";
var body_text05_jpn = "タッチデバイス";
var body_text06_jpn = "広告 デバイス";
var body_text07_jpn = "広告 バージョンアップ";
var body_text08_jpn = "ファイルの添付";
var body_text09_jpn = "ファイルリスト";
var body_text10_jpn = "すべて";
var body_text11_jpn = "画像";
var body_text12_jpn = "映像";
var body_text13_jpn = "適用";

//본문 버튼
var body_button01_kor = "검색";
var body_button02_kor = "등록";
var body_button03_kor = "저장";
var body_button04_kor = "삭제";
var body_button05_kor = "목록";
var body_button06_kor = "재시작";
var body_button07_kor = "재부팅";
var body_button08_kor = "종료";
var body_button09_kor = "편집";
var body_button10_kor = "버전 업데이트";
var body_button06_s_kor = "재시작";
var body_button07_s_kor = "재부팅";
var body_button08_s_kor = "종료";
var body_button11_s_kor = "파일 수정";
var body_button12_s_kor = "파일 삭제";
var body_button13_s_kor = "다운로드";

var body_button01_eng = "Search";
var body_button02_eng = "Create";
var body_button03_eng = "Save";
var body_button04_eng = "Delete";
var body_button05_eng = "List";
var body_button06_eng = "Reload";
var body_button07_eng = "Reboot";
var body_button08_eng = "Exit";
var body_button09_eng = "Move";
var body_button10_eng = "Version update";
var body_button06_s_eng = "Reload";
var body_button07_s_eng = "Reboot";
var body_button08_s_eng = "Exit";
var body_button11_s_eng = "File modify";
var body_button12_s_eng = "File delete";
var body_button13_s_eng = "Download";

var body_button01_chn = "搜索";
var body_button02_chn = "创建";
var body_button03_chn = "保存";
var body_button04_chn = "删除";
var body_button05_chn = "名单";
var body_button06_chn = "刷新";
var body_button07_chn = "重启";
var body_button08_chn = "关掉";
var body_button09_chn = "移动";
var body_button10_chn = "版本更新";
var body_button06_s_chn = "刷新";
var body_button07_s_chn = "重启";
var body_button08_s_chn = "关掉";
var body_button11_s_chn = "文件修改";
var body_button12_s_chn = "文件删除";
var body_button13_s_chn = "下载";

var body_button01_jpn = "サーチ";
var body_button02_jpn = "作り出します";
var body_button03_jpn = "セーブ";
var body_button04_jpn = "削除";
var body_button05_jpn = "リスト";
var body_button06_jpn = "リロード";
var body_button07_jpn = "リブート";
var body_button08_jpn = "シャットダウン";
var body_button09_jpn = "移動";
var body_button10_jpn = "バージョンアップ";
var body_button06_s_jpn = "リロード";
var body_button07_s_jpn = "リブート";
var body_button08_s_jpn = "シャットダウン";
var body_button11_s_jpn = "修正ファイルが";
var body_button12_s_jpn = "ファイル削除";
var body_button13_s_jpn = "ダウンロード";


function setChangeLang(type){
    if(type == "KOR"){
        $("#id_profile_txt").html(top_profile_kor);
        $("#id_logout_txt").html(top_logout_kor);
        $("#id_notice_txt").html(top_notice_kor);

        $("#id_main_dash_txt").html(left_main_dash_kor);
        $(".id_main_upload_txt").html(left_main_upload_kor);
        $(".id_main_device_txt").html(left_main_device_kor);

        $(".id_sub_contents_txt").html(left_sub_contents_kor);
        $(".id_sub_schedule_txt").html(left_sub_schedule_kor);
        $(".id_sub_calendar_txt").html(left_sub_calendar_kor);
        $(".id_sub_monitoring_txt").html(left_sub_monitoring_kor);
        $(".id_sub_device_txt").html(left_sub_device_kor);
        $(".id_sub_event_txt").html(left_sub_event_kor);
        $(".id_sub_store_txt").html(left_sub_store_kor);
        $(".id_sub_public_txt").html(left_sub_public_kor);
        $(".id_sub_stats_txt").html(left_sub_stats_kor);
        $(".id_sub_log_txt").html(left_sub_log_kor);
        $(".id_sub_program_txt").html(left_sub_program_kor);
        $(".id_sub_user_txt").html(left_sub_user_kor);

        $("#id_title01_txt").html(right_title01_kor);
        $("#id_title02_txt").html(right_title02_kor);
        $("#id_title03_txt").html(right_title03_kor);

        $("#id_text01_txt").html(body_text01_kor);
        $("#id_text02_txt").html(body_text02_kor);
        $("#id_text03_txt").html(body_text03_kor);
        $("#id_text04_txt").html(body_text04_kor);
        $("#id_text05_txt").html(body_text05_kor);
        $("#id_text06_txt").html(body_text06_kor);
        $("#id_text07_txt").html(body_text07_kor);
        $("#id_text08_txt").html(body_text08_kor);
        $("#id_text09_txt").html(body_text09_kor);
        $("#id_text10_txt").html(body_text10_kor);
        $("#id_text11_txt").html(body_text11_kor);
        $("#id_text12_txt").html(body_text12_kor);
        $("#id_text13_txt").html(body_text13_kor);

        $("#id_button01_txt").html(body_button01_kor);
        $("#id_button02_txt").html(body_button02_kor);
        $("#id_button03_txt").html(body_button03_kor);
        $("#id_button04_txt").html(body_button04_kor);
        $("#id_button05_txt").html(body_button05_kor);
        $(".id_button06_txt").html(body_button06_kor);
        $(".id_button07_txt").html(body_button07_kor);
        $(".id_button08_txt").html(body_button08_kor);
        $(".id_button09_txt").html(body_button09_kor);
        $(".id_button10_txt").html(body_button10_kor);
        $(".id_button06_s_txt").html(body_button06_s_kor);
        $(".id_button07_s_txt").html(body_button07_s_kor);
        $(".id_button08_s_txt").html(body_button08_s_kor);
        $(".id_button11_s_txt").html(body_button11_s_kor);
        $(".id_button12_s_txt").html(body_button12_s_kor);
        $(".id_button13_s_txt").html(body_button13_s_kor);
    }else if(type == "ENG"){
        $("#id_profile_txt").html(top_profile_eng);
        $("#id_logout_txt").html(top_logout_eng);
        $("#id_notice_txt").html(top_notice_eng);

        $("#id_main_dash_txt").html(left_main_dash_eng);
        $(".id_main_upload_txt").html(left_main_upload_eng);
        $(".id_main_device_txt").html(left_main_device_eng);

        $(".id_sub_contents_txt").html(left_sub_contents_eng);
        $(".id_sub_schedule_txt").html(left_sub_schedule_eng);
        $(".id_sub_calendar_txt").html(left_sub_calendar_eng);
        $(".id_sub_monitoring_txt").html(left_sub_monitoring_eng);
        $(".id_sub_device_txt").html(left_sub_device_eng);
        $(".id_sub_event_txt").html(left_sub_event_eng);
        $(".id_sub_store_txt").html(left_sub_store_eng);
        $(".id_sub_public_txt").html(left_sub_public_eng);
        $(".id_sub_stats_txt").html(left_sub_stats_eng);
        $(".id_sub_log_txt").html(left_sub_log_eng);
        $(".id_sub_program_txt").html(left_sub_program_eng);
        $(".id_sub_user_txt").html(left_sub_user_eng);

        $("#id_title01_txt").html(right_title01_eng);
        $("#id_title02_txt").html(right_title02_eng);
        $("#id_title03_txt").html(right_title03_eng);

        $("#id_text01_txt").html(body_text01_eng);
        $("#id_text02_txt").html(body_text02_eng);
        $("#id_text03_txt").html(body_text03_eng);
        $("#id_text04_txt").html(body_text04_eng);
        $("#id_text05_txt").html(body_text05_eng);
        $("#id_text06_txt").html(body_text06_eng);
        $("#id_text07_txt").html(body_text07_eng);
        $("#id_text08_txt").html(body_text08_eng);
        $("#id_text09_txt").html(body_text09_eng);
        $("#id_text10_txt").html(body_text10_eng);
        $("#id_text11_txt").html(body_text11_eng);
        $("#id_text12_txt").html(body_text12_eng);
        $("#id_text13_txt").html(body_text13_eng);


        $("#id_button01_txt").html(body_button01_eng);
        $("#id_button02_txt").html(body_button02_eng);
        $("#id_button03_txt").html(body_button03_eng);
        $("#id_button04_txt").html(body_button04_eng);
        $("#id_button05_txt").html(body_button05_eng);
        $(".id_button06_txt").html(body_button06_eng);
        $(".id_button07_txt").html(body_button07_eng);
        $(".id_button08_txt").html(body_button08_eng);
        $(".id_button09_txt").html(body_button09_eng);
        $(".id_button10_txt").html(body_button10_eng);
        $(".id_button06_s_txt").html(body_button06_s_eng);
        $(".id_button07_s_txt").html(body_button07_s_eng);
        $(".id_button08_s_txt").html(body_button08_s_eng);
        $(".id_button11_s_txt").html(body_button11_s_eng);
        $(".id_button12_s_txt").html(body_button12_s_eng);
        $(".id_button13_s_txt").html(body_button13_s_eng);
    }else if(type == "CHN"){
        $("#id_profile_txt").html(top_profile_chn);
        $("#id_logout_txt").html(top_logout_chn);
        $("#id_notice_txt").html(top_notice_chn);

        $("#id_main_dash_txt").html(left_main_dash_chn);
        $(".id_main_upload_txt").html(left_main_upload_chn);
        $(".id_main_device_txt").html(left_main_device_chn);

        $(".id_sub_contents_txt").html(left_sub_contents_chn);
        $(".id_sub_schedule_txt").html(left_sub_schedule_chn);
        $(".id_sub_calendar_txt").html(left_sub_calendar_chn);
        $(".id_sub_monitoring_txt").html(left_sub_monitoring_chn);
        $(".id_sub_device_txt").html(left_sub_device_chn);
        $(".id_sub_event_txt").html(left_sub_event_chn);
        $(".id_sub_store_txt").html(left_sub_store_chn);
        $(".id_sub_public_txt").html(left_sub_public_chn);
        $(".id_sub_stats_txt").html(left_sub_stats_chn);
        $(".id_sub_log_txt").html(left_sub_log_chn);
        $(".id_sub_program_txt").html(left_sub_program_chn);
        $(".id_sub_user_txt").html(left_sub_user_chn);

        $("#id_title01_txt").html(right_title01_chn);
        $("#id_title02_txt").html(right_title02_chn);
        $("#id_title03_txt").html(right_title03_chn);

        $("#id_text01_txt").html(body_text01_chn);
        $("#id_text02_txt").html(body_text02_chn);
        $("#id_text03_txt").html(body_text03_chn);
        $("#id_text04_txt").html(body_text04_chn);
        $("#id_text05_txt").html(body_text05_chn);
        $("#id_text06_txt").html(body_text06_chn);
        $("#id_text07_txt").html(body_text07_chn);
        $("#id_text08_txt").html(body_text08_chn);
        $("#id_text09_txt").html(body_text09_chn);
        $("#id_text10_txt").html(body_text10_chn);
        $("#id_text11_txt").html(body_text11_chn);
        $("#id_text12_txt").html(body_text12_chn);
        $("#id_text13_txt").html(body_text13_chn);

        $("#id_button01_txt").html(body_button01_chn);
        $("#id_button02_txt").html(body_button02_chn);
        $("#id_button03_txt").html(body_button03_chn);
        $("#id_button04_txt").html(body_button04_chn);
        $("#id_button05_txt").html(body_button05_chn);
        $(".id_button06_txt").html(body_button06_chn);
        $(".id_button07_txt").html(body_button07_chn);
        $(".id_button08_txt").html(body_button08_chn);
        $(".id_button09_txt").html(body_button09_chn);
        $(".id_button10_txt").html(body_button10_chn);
        $(".id_button06_s_txt").html(body_button06_s_chn);
        $(".id_button07_s_txt").html(body_button07_s_chn);
        $(".id_button08_s_txt").html(body_button08_s_chn);
        $(".id_button11_s_txt").html(body_button11_s_chn);
        $(".id_button12_s_txt").html(body_button12_s_chn);
        $(".id_button13_s_txt").html(body_button13_s_chn);
    }else if(type == "JPN"){
        $("#id_profile_txt").html(top_profile_jpn);
        $("#id_logout_txt").html(top_logout_jpn);
        $("#id_notice_txt").html(top_notice_jpn);

        $("#id_main_dash_txt").html(left_main_dash_jpn);
        $(".id_main_upload_txt").html(left_main_upload_jpn);
        $(".id_main_device_txt").html(left_main_device_jpn);

        $(".id_sub_contents_txt").html(left_sub_contents_jpn);
        $(".id_sub_schedule_txt").html(left_sub_schedule_jpn);
        $(".id_sub_calendar_txt").html(left_sub_calendar_jpn);
        $(".id_sub_monitoring_txt").html(left_sub_monitoring_jpn);
        $(".id_sub_device_txt").html(left_sub_device_jpn);
        $(".id_sub_event_txt").html(left_sub_event_jpn);
        $(".id_sub_store_txt").html(left_sub_store_jpn);
        $(".id_sub_public_txt").html(left_sub_public_jpn);
        $(".id_sub_stats_txt").html(left_sub_stats_jpn);
        $(".id_sub_log_txt").html(left_sub_log_jpn);
        $(".id_sub_program_txt").html(left_sub_program_jpn);
        $(".id_sub_user_txt").html(left_sub_user_jpn);

        $("#id_title01_txt").html(right_title01_jpn);
        $("#id_title02_txt").html(right_title02_jpn);
        $("#id_title03_txt").html(right_title03_jpn);

        $("#id_text01_txt").html(body_text01_jpn);
        $("#id_text02_txt").html(body_text02_jpn);
        $("#id_text03_txt").html(body_text03_jpn);
        $("#id_text04_txt").html(body_text04_jpn);
        $("#id_text05_txt").html(body_text05_jpn);
        $("#id_text06_txt").html(body_text06_jpn);
        $("#id_text07_txt").html(body_text07_jpn);
        $("#id_text08_txt").html(body_text08_jpn);
        $("#id_text09_txt").html(body_text09_jpn);
        $("#id_text10_txt").html(body_text10_jpn);
        $("#id_text11_txt").html(body_text11_jpn);
        $("#id_text12_txt").html(body_text12_jpn);
        $("#id_text13_txt").html(body_text13_jpn);

        $("#id_button01_txt").html(body_button01_jpn);
        $("#id_button02_txt").html(body_button02_jpn);
        $("#id_button03_txt").html(body_button03_jpn);
        $("#id_button04_txt").html(body_button04_jpn);
        $("#id_button05_txt").html(body_button05_jpn);
        $(".id_button06_txt").html(body_button06_jpn);
        $(".id_button07_txt").html(body_button07_jpn);
        $(".id_button08_txt").html(body_button08_jpn);
        $(".id_button09_txt").html(body_button09_jpn);
        $(".id_button10_txt").html(body_button10_jpn);
        $(".id_button06_s_txt").html(body_button06_s_jpn);
        $(".id_button07_s_txt").html(body_button07_s_jpn);
        $(".id_button08_s_txt").html(body_button08_s_jpn);
        $(".id_button11_s_txt").html(body_button11_s_jpn);
        $(".id_button12_s_txt").html(body_button12_s_jpn);
        $(".id_button13_s_txt").html(body_button13_s_jpn);
    }
}